package com.bitkirin.kirinbbs.service;

import com.bitkirin.kirinbbs.domain.Reply;

/**
 * @program: BBS_Kirin
 * @description
 * @author: Dcy
 * @create: 2020-04-28 17:24
 **/
public interface ReplyService {
	void add(Reply reply);

	void deleteReply(Reply reply);
}
