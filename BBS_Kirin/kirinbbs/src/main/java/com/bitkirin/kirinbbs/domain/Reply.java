package com.bitkirin.kirinbbs.domain;

/**
 * @program: BBS_Kirin
 * @description
 * @author: Dcy
 * @create: 2020-04-28 15:55
 **/
public class Reply {
	private Integer id;				//回复id
	private Integer post_id;		//帖子id
	private Integer user_id;
	private Integer replyuser_id;
	private String content;
	private Integer parse_count;
	private String create_time;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPost_id() {
		return post_id;
	}

	public void setPost_id(Integer post_id) {
		this.post_id = post_id;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public Integer getReplyuser_id() {
		return replyuser_id;
	}

	public void setReplyuser_id(Integer replyuser_id) {
		this.replyuser_id = replyuser_id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getParse_count() {
		return parse_count;
	}

	public void setParse_count(Integer parse_count) {
		this.parse_count = parse_count;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
}
