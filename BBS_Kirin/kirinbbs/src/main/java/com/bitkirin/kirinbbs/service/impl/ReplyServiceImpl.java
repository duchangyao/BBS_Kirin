package com.bitkirin.kirinbbs.service.impl;

import com.bitkirin.kirinbbs.domain.Reply;
import com.bitkirin.kirinbbs.mapper.ReplyMapper;
import com.bitkirin.kirinbbs.service.ReplyService;
import com.bitkirin.kirinbbs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @program: BBS_Kirin
 * @description
 * @author: Dcy
 * @create: 2020-04-28 17:26
 **/
@Service
public class ReplyServiceImpl implements ReplyService {

	@Autowired
	private ReplyMapper replyMapper;
	@Autowired
	private UserService userService;

	@Override
	@Transactional
	public void add(Reply reply) {
		replyMapper.add(reply);
		//添加用户积分
		userService.replyAddIntegral(reply);
	}

	@Override
	@Transactional
	public void deleteReply(Reply reply) {
		//需要判断当前用户是否删除的是自己的回复

		if (reply.getId()==null){
			return;
		}
		if (reply.getUser_id()==null){
			return;
		}
		replyMapper.deleteReply(reply.getId());
	}
}
