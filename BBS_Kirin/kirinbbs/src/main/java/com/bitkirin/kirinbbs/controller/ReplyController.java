package com.bitkirin.kirinbbs.controller;

import com.bitkirin.kirinbbs.domain.Reply;
import com.bitkirin.kirinbbs.entity.Result;
import com.bitkirin.kirinbbs.service.ReplyService;
import com.bitkirin.kirinbbs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: BBS_Kirin
 * @description
 * @author: Dcy
 * @create: 2020-04-28 17:19
 **/
@RestController
@RequestMapping("/reply")
public class ReplyController {

	@Autowired
	private ReplyService replyService;
	@Autowired
	private UserService userService;

	/**
	 * 添加回复
	 */
	@RequestMapping("/add")
	public Result replyPost(@RequestBody Reply reply){
		if (reply.getContent()==null){
			return new Result(false,"回复内容不能为空");
		}
		if (reply.getContent().length()>50){
			return new Result(false,"回复内容最多50字");
		}
		reply.setParse_count(0);	//默认点赞数为0
		try {
			replyService.add(reply);
			return new Result(true,"回复成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false,"回复失败");
		}
	}

	/**
	 * 删除回复
	 */
	@RequestMapping("deleteReply")
	public Result deleteReply(Reply reply){
		//现根据用户的id查看用户积分是否足够
		Integer integral = userService.findIntegral(reply.getUser_id());
		if (integral>=1){
			try {
				replyService.deleteReply(reply);
				userService.updateIntegral(reply.getUser_id(),-1);
				return new Result(true,"删除成功,积分-1");
			} catch (Exception e) {
				e.printStackTrace();
				return new Result(false,"删除失败");
			}
		}else {
			return new Result(false,"删除失败，积分不足");
		}
	}

}
