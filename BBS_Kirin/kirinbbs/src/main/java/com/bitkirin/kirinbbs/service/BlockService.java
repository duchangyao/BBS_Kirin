package com.bitkirin.kirinbbs.service;

import com.bitkirin.kirinbbs.domain.Block;
import com.bitkirin.kirinbbs.entity.PageResult;
import com.bitkirin.kirinbbs.entity.QueryPageBean;

import java.util.List;

public interface BlockService {
	PageResult pageQuery(QueryPageBean queryPageBean);

	void add(Block block);

	boolean delete(Integer id);

	void edit(Block block);

	Block findById(Integer id);

	List<String> findParentMenuIds();

	List<Block> findParentMenu();

	List<Block> findChildrenMenu(Integer id);

	List<Block> findMenuByUsername(String username);

	Block findByName(String name);
}
