package com.bitkirin.kirinbbs.service.impl;

import com.bitkirin.kirinbbs.domain.Block;
import com.bitkirin.kirinbbs.entity.PageResult;
import com.bitkirin.kirinbbs.entity.QueryPageBean;
import com.bitkirin.kirinbbs.mapper.BlockMapper;
import com.bitkirin.kirinbbs.service.BlockService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import com.github.pagehelper.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BlockServiceImpl implements BlockService {
	@Autowired
	private BlockMapper blockMapper;

	@Override
	public PageResult pageQuery(QueryPageBean queryPageBean) {
		Integer currentPage = queryPageBean.getCurrentPage();
		Integer pageSize = queryPageBean.getPageSize();
		String queryString = queryPageBean.getQueryString();//查询条件
		//完成分页查询，基于mybatis框架提供的分页助手插件完成
		PageHelper.startPage(currentPage,pageSize);
		//select * from block limit 0,10
		Page<Block> page =  blockMapper.findParentMenu(queryString);
		long total = page.getTotal();
		List<Block> rows = page.getResult();
		return new PageResult(total,rows);
	}

	@Override
	@Transactional
	public void add(Block block) {
		blockMapper.add(block);
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		try {
			blockMapper.deleteMenuById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	@Transactional
	public void edit(Block block) {
		blockMapper.edit(block);
	}

	@Override
	public Block findById(Integer id) {
		return blockMapper.findById(id);
	}

	@Override
	public List<String> findParentMenuIds() {
		return blockMapper.findParentMenuIds();
	}

	@Override
	public List<Block> findParentMenu() {
		return blockMapper.findParentMenuMap();
	}

	@Override
	public List<Block> findChildrenMenu(Integer id) {
		return blockMapper.findChildrenMenu(id);
	}

	@Override
	public List<Block> findMenuByUsername(String username) {
		//获取对应的一级菜单
		List<Block> parentMenu = blockMapper.findLv1Menu();
		//根据一级菜单的id,获取对应的二级菜单
		for (Block block : parentMenu) {
			List<Block> childrenMenu = blockMapper.findMenuByMenuId(block.getId());
			//把二级菜单存入一级菜单
			block.setChildren(childrenMenu);
		}
		return parentMenu;
	}

	@Override
	public Block findByName(String name) {
		return blockMapper.findByName(name);
	}


}
