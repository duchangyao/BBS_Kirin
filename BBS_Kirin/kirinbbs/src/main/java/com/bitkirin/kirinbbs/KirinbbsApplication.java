package com.bitkirin.kirinbbs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KirinbbsApplication {

	public static void main(String[] args) {
		SpringApplication.run(KirinbbsApplication.class, args);
	}

}
