package com.bitkirin.kirinbbs.service;

import com.bitkirin.kirinbbs.domain.Reply;
import com.bitkirin.kirinbbs.domain.User;

import java.util.ArrayList;

/**
 * @program: BBS_Kirin
 * @description
 * @author: Dcy
 * @create: 2020-04-23 12:09
 **/
public interface UserService {
	ArrayList<User> findAll();

	/**
	 * 积分查询方法
	 */
	Integer findIntegral(Integer id);

	/**
	 * 恢复后积分添加
	 * @param reply
	 */
	void replyAddIntegral(Reply reply);

	void updateIntegral(Integer user_id,Integer integral);
}
