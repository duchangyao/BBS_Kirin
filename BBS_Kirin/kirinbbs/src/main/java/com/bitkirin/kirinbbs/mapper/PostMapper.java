package com.bitkirin.kirinbbs.mapper;


import com.bitkirin.kirinbbs.domain.Post;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;


/**
 * @program: BBS_Kirin
 * @description
 * @author: Dcy
 * @create: 2020-04-27 16:31
 **/
@Mapper
public interface PostMapper {

	@Select("select p.id,p.block_id,p.user_id,p.title,p.content,p.create_time,p.pars_count,p.priority,u.username from post p,t_user u where p.user_id=u.id ORDER BY p.pars_count DESC")
	Page<Post> findAllPost(String queryString);

	@Select("SELECT p.id,p.block_id,p.user_id,p.title,p.content,p.create_time,p.pars_count,p.priority,u.username " +
			"FROM post p,t_user u,block b " +
			"WHERE p.block_id=b.id AND p.user_id = u.id AND b.name=#{block}" +
			"ORDER BY p.pars_count DESC")
	Page<Post> findByBlock(String block);

	/**
	 * 根据id删除帖子
	 * @param id
	 * @return
	 */
	@Delete("delete from post where id = #{id}")
	Integer deletePost(Integer id);

	/**
	 * 创建新帖子
	 * @param post
	 */
	@Insert("INSERT INTO post (id,block_id,user_id,title,content,create_time,pars_count,state,priority)\n" +
			"        VALUES\n" +
			"        (#{id},#{block_id},#{user_id},#{title},#{content},#{create_time},#{pars_count},#{state},#{priority})")
	void createPost(Post post);
}
