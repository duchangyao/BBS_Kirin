package com.bitkirin.kirinbbs.mapper;

import com.bitkirin.kirinbbs.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.ArrayList;

/**
 * @program: BBS_Kirin
 * @description
 * @author: Dcy
 * @create: 2020-04-23 12:07
 **/
@Mapper
public interface UserMapper {
	@Select("SELECT * FROM t_user")
	ArrayList<User> findAll();

	@Select("SELECT integral FROM t_user WHERE id=#{id}")
	User findIntegral(Integer id);

	/**
	 * 创建或删除帖子更新积分操作
	 * @param integral
	 */
	@Update("UPDATE t_user SET integral=integral+#{integral} WHERE id = #{user_id}")
	void updateIntegral(@Param("user_id")Integer user_id,@Param("integral") Integer integral);

	/**
	 * 给回复者添加2积分
	 * @param user_id
	 * @param integral
	 */
	@Update("UPDATE t_user SET integral=integral+#{integral} WHERE id = #{user_id}")
	void addReplyIntegral(@Param("user_id")Integer user_id, @Param("integral")Integer integral);

	/**
	 * 给发帖人（被回复者）添加5积分
	 * @param replyuser_id
	 * @param integral
	 */
	@Update("UPDATE t_user SET integral=integral+#{integral} WHERE id = #{replyuser_id}")
	void addPostIntegral(@Param("replyuser_id") Integer replyuser_id, @Param("integral") Integer integral);

}
