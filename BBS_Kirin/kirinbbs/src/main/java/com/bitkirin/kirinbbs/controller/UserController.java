package com.bitkirin.kirinbbs.controller;

import com.bitkirin.kirinbbs.domain.User;
import com.bitkirin.kirinbbs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * @program: BBS_Kirin
 * @description
 * @author: Dcy
 * @create: 2020-04-23 12:06
 **/
@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;

	@RequestMapping("/findAll")
	@ResponseBody
	public ArrayList<User> findAll(){
		ArrayList<User> all = userService.findAll();
		return all;
	}
}
