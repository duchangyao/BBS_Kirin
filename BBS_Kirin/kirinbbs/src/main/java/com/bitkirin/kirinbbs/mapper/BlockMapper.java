package com.bitkirin.kirinbbs.mapper;

import com.bitkirin.kirinbbs.domain.Block;
import com.bitkirin.kirinbbs.domain.User;
import org.apache.ibatis.annotations.*;
import com.github.pagehelper.Page;

import java.util.List;

@Mapper
public interface BlockMapper {

	Page<Block> selectByCondition(String queryString);

	/**
	 * 添加板块
	 * @param block
	 */
	@Insert("insert into block (id,name,linkUrl,path,priority,icon,description,parentMenuId,level)\n" +
			"        values\n" +
			"        (#{id},#{name},#{linkUrl},#{path},#{priority},#{icon},#{description},#{parentMenuId},#{level})")
	void add(Block block);

	/**
	 * 根据id删除板块
	 * @param id
	 */
	@Delete("delete from block where id = #{id}")
	void deleteMenuById(Integer id);

	/**
	 * 根据id查询板块详情
	 * @param id
	 * @return
	 */
	@Select("select * from block where id = #{id}")
	Block findById(Integer id);

	/**
	 * 不确定用户到底改了什么，使用动态sql
	 * @param block
	 */
	@Update("<script>update block\n" +
			"        <set>\n" +
			"            <if test=\"name != null\">\n" +
			"                name = #{name},\n" +
			"            </if>\n" +
			"            <if test=\"linkUrl != null\">\n" +
			"                linkUrl = #{linkUrl},\n" +
			"            </if>\n" +
			"            <if test=\"path != null\">\n" +
			"                path = #{path},\n" +
			"            </if>\n" +
			"            <if test=\"priority != null\">\n" +
			"                priority = #{priority},\n" +
			"            </if>\n" +
			"            <if test=\"parentMenuId != null\">\n" +
			"                parentMenuId = #{parentMenuId},\n" +
			"            </if>\n" +
			"            <if test=\"level != null\">\n" +
			"                level = #{level},\n" +
			"            </if>\n" +
			"        </set>\n" +
			"        where id = #{id}" +
			"</script>")
	void edit(Block block);

	@Select("SELECT id FROM block WHERE parentMenuId IS NULL")
	List<String> findParentMenuIds();

	@Select("select id,name,path,icon,linkUrl,level,parentMenuId,icon from block where level = 1")
	Page<Block> findParentMenu(String queryString);
	@Select("select id,name,path,icon,linkUrl,level,parentMenuId,icon from block where level = 1 order by priority")
	List<Block> findParentMenuMap();
	@Select("select id,name,path,icon,linkUrl,level,parentMenuId,icon from block where level = 2 and parentMenuId = #{id} order by priority")
	List<Block> findChildrenMenu(Integer id);


	/**
	 * 查询一级菜单
	 * @return
	 */
	@Select("select id,name,path,icon,linkUrl from block where level = 1 order by priority ASC")
	List<Block> findLv1Menu();

	/**
	 * 根据一级菜单的id查询所有的二级菜单
	 * @param menuId
	 * @return
	 */
	@Select("select id,name,path,icon,linkUrl from block where parentMenuId=#{menuId}")
	List<Block> findMenuByMenuId(@Param("menuId") Integer menuId);

	@Select("select id,name from block where name = #{name}")
	Block findByName(String name);
}
