package com.bitkirin.kirinbbs.service.impl;

import com.bitkirin.kirinbbs.domain.Reply;
import com.bitkirin.kirinbbs.domain.User;
import com.bitkirin.kirinbbs.mapper.UserMapper;
import com.bitkirin.kirinbbs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * @program: BBS_Kirin
 * @description
 * @author: Dcy
 * @create: 2020-04-23 12:10
 **/
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserMapper userMapper;

	@Override
	public ArrayList<User> findAll() {
		ArrayList<User> users = userMapper.findAll();
		return users;
	}

	@Override
	public Integer findIntegral(Integer id) {
		User user = userMapper.findIntegral(id);
		return user.getIntegral();
	}

	/**
	 * 回复后增加积分
	 * @param reply
	 */
	@Override
	@Transactional
	public void replyAddIntegral(Reply reply) {
		Integer user_id = reply.getUser_id();	//回复者id
		Integer replyuser_id = reply.getReplyuser_id(); 	//发帖人id
		userMapper.addReplyIntegral(user_id,2);	//给回复人增加积分
		userMapper.addPostIntegral(replyuser_id,5);	//给发帖人增加积分
	}

	@Override
	@Transactional
	public void updateIntegral(Integer user_id, Integer integral) {
		if (user_id==null){
			return;
		}
		userMapper.updateIntegral(user_id,integral);
	}
}
