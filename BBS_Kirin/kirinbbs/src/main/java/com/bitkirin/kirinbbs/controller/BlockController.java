package com.bitkirin.kirinbbs.controller;
/**
 * @program: BBS_Kirin
 * @description
 * @author: Dcy
 * @create: 2020-04-27 10:42
 **/
import com.bitkirin.kirinbbs.domain.Block;
import com.bitkirin.kirinbbs.entity.PageResult;
import com.bitkirin.kirinbbs.entity.QueryPageBean;
import com.bitkirin.kirinbbs.entity.Result;
import com.bitkirin.kirinbbs.service.BlockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/block")
public class BlockController {
	@Autowired
	private BlockService blockService;

	//查询所有的可以被自关联的父板块id
	@RequestMapping("/findParentBlockIds")
	public Result findParentBlockIds(){
		try {
			List<String> list = blockService.findParentMenuIds();
			return new Result(true,"查找成功",list);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false,"查找成功");
		}
	}

	//查询所有一级菜单信息
	@RequestMapping("/findParentBlock")
	public Result findParentBlock(){
		try {
			List<Block> BlockList = blockService.findParentMenu();
			return new Result(true,"查询成功",BlockList);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false,"查询失败");
		}
	}

	//检查项分页查询
	@RequestMapping("/findPage")
	public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
		PageResult pageResult = blockService.pageQuery(queryPageBean);
		return pageResult;
	}

	//添加菜单信息
	@RequestMapping("/add")
	public Result add(@RequestBody Block block){
		try {
			Block block1 = blockService.findByName(block.getName());
			if (block1 == null){
				blockService.add(block);
			}else {
				return new Result(false,"菜单名重复,请换一个");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "添加板块成功");
		}
		return new Result(true,"添加板块失败");
	}

	//删除菜单
	@RequestMapping("/delete")
	public Result delete(Integer id){
		try {
			boolean flag = blockService.delete(id);
			if (!flag){
				return new Result(false,"删除失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败");
		}
		return new Result(true,"删除成功");
	}

	//修改菜单信息
	@RequestMapping("/edit")
	public Result edit(@RequestBody Block block){
		try {
			blockService.edit(block);
			return new Result(true,"修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false,"修改失败");
		}
	}

	//根据id查询菜单信息
	@RequestMapping("/findById")
	public Result findById(Integer id){
		try {
			Block Block = blockService.findById(id);
			return new Result(true,"查询成功",Block);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false,"查询失败");
		}
	}

	/**
	 * 查询所有板块
	 * @return
	 */
	@RequestMapping("/blockAll")
	public Result BlockAll(){
		try {
			String username = "xiaoming";
			List<Block> blockList = blockService.findMenuByUsername(username);
			Map<String, Object> map = new HashMap<>();
			map.put("username",username);
			map.put("blockList",blockList);
			return new Result(true,"查询成功",map);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false,"查询失败");
		}
	}

	@RequestMapping("/findByName")
	public Result findByName(@RequestBody String name){
		try {
			Block block = blockService.findByName(name);
			return new Result(true,"查询成功",block);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false,"查询失败");
		}
	}
}
