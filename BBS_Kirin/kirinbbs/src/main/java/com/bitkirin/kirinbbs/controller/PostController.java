package com.bitkirin.kirinbbs.controller;

import com.bitkirin.kirinbbs.domain.Post;
import com.bitkirin.kirinbbs.entity.PageResult;
import com.bitkirin.kirinbbs.entity.QueryPageBean;
import com.bitkirin.kirinbbs.entity.Result;
import com.bitkirin.kirinbbs.service.PostService;
import com.bitkirin.kirinbbs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @program: BBS_Kirin
 * @description
 * @author: Dcy
 * @create: 2020-04-27 16:19
 **/
@RestController
@RequestMapping("/post")
public class PostController {

	@Autowired
	private PostService postService;
	@Autowired
	private UserService userService;

	//所有板块所有帖子分页查询
	@RequestMapping("/findPage")
	public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
		PageResult pageResult = postService.pageQuery(queryPageBean);
		return pageResult;
	}

	//查询每个板块的所有帖子
	@GetMapping("/{block}")
	public PageResult findByBlock(@PathVariable String block){
		PageResult pageResult = postService.findByBlock(block);
		return pageResult;
	}

	/**
	 * 根据id，删除帖子
	 */
	@RequestMapping("/delPost")
	public Result delPost(@RequestBody Post post){
		//现根据用户的id查看用户积分是否足够
		Integer integral = userService.findIntegral(post.getUser_id());
		if (integral-1>=0){
			//积分足够执行删除和扣减操作
			try {
				postService.delPost(post.getId(),-1);
				return new Result(true,"删除成功,并删除1积分");
			} catch (Exception e) {
				e.printStackTrace();
				return new Result(false,"删除失败");
			}
		}else {
			return new Result(false,"积分不足,删除失败");
		}

	}

	/**
	 * 创建帖子
	 */
	@RequestMapping("/add")
	public Result createPost(@RequestBody Post post){
		if (post!=null&&post.getContent()!=null&&post.getTitle()!=null){
			post.setUsername("xiaoming");
			post.setUser_id(2);
			post.setPars_count(0);	//新建帖子点赞数为0
			post.setState(1);		//审核状态暂定通过
			try {
				postService.createPost(post);
				return new Result(true,"帖子发布成功，+10积分");
			} catch (Exception e) {
				e.printStackTrace();
				return new Result(false,"创建失败");
			}
		}else {
			return new Result(false,"创建失败");
		}
	}
}
