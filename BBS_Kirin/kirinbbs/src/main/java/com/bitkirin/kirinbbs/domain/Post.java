package com.bitkirin.kirinbbs.domain;

/**
 * @program: BBS_Kirin
 * @description
 * @author: Dcy
 * @create: 2020-04-27 16:09
 **/

import java.io.Serializable;
import java.util.Date;

/**
 * 帖子
 */
public class Post implements Serializable {
	private Integer id; 	     //帖子id
	private Integer block_id;    //所属板块id
	private Integer user_id;     //发帖人id
	private String username;	 //发帖人姓名
	private String title;		 //标题
	private String content;		 //内容
	private String create_time;	 //发帖时间
	private Integer pars_count;  //点赞数
	private Integer state;		 //评论状态
	private Integer priority;	 //排序值
	private String img; 		 //图片

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBlock_id() {
		return block_id;
	}

	public void setBlock_id(Integer block_id) {
		this.block_id = block_id;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public Integer getPars_count() {
		return pars_count;
	}

	public void setPars_count(Integer pars_count) {
		this.pars_count = pars_count;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}
}
