package com.bitkirin.kirinbbs.service;

import com.bitkirin.kirinbbs.domain.Block;
import com.bitkirin.kirinbbs.domain.Post;
import com.bitkirin.kirinbbs.entity.PageResult;
import com.bitkirin.kirinbbs.entity.QueryPageBean;
import com.github.pagehelper.Page;

import java.util.List;

/**
 * @program: BBS_Kirin
 * @description
 * @author: Dcy
 * @create: 2020-04-27 16:29
 **/
public interface PostService {

	PageResult pageQuery(QueryPageBean queryPageBean);

	PageResult findByBlock(String block);

	boolean deletePost(Post post,Integer integral);

	void createPost(Post post);
}
