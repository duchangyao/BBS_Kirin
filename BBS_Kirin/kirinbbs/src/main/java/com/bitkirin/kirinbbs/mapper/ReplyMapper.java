package com.bitkirin.kirinbbs.mapper;

import com.bitkirin.kirinbbs.domain.Reply;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: BBS_Kirin
 * @description
 * @author: Dcy
 * @create: 2020-04-28 17:28
 **/
@Mapper
public interface ReplyMapper {
	@Insert("INSERT INTO post_reply (post_id,user_id,replyuser_id,content,parse_count,create_time)" +
			"VALUES" +
			"(#{post_id},#{user_id},#{replyuser_id},#{content},#{parse_count},#{create_time})")
	void add(Reply reply);

	/**
	 * 根据id删除回复
	 * @param id 回复id
	 */
	@Delete("DELETE FROM post_reply WHERE id = #{id}")
	void deleteReply(Integer id);
}
