package com.bitkirin.kirinbbs.service.impl;

import com.bitkirin.kirinbbs.domain.Block;
import com.bitkirin.kirinbbs.domain.Post;
import com.bitkirin.kirinbbs.entity.PageResult;
import com.bitkirin.kirinbbs.entity.QueryPageBean;
import com.bitkirin.kirinbbs.entity.Result;
import com.bitkirin.kirinbbs.mapper.PostMapper;
import com.bitkirin.kirinbbs.mapper.UserMapper;
import com.bitkirin.kirinbbs.service.PostService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @program: BBS_Kirin
 * @description
 * @author: Dcy
 * @create: 2020-04-27 16:29
 **/
@Service
public class PostServiceImpl implements PostService {

	@Autowired
	private PostMapper postMapper;
	@Autowired
	private UserMapper userMapper;

	@Override
	public PageResult pageQuery(QueryPageBean queryPageBean) {
		Integer currentPage = queryPageBean.getCurrentPage();
		Integer pageSize = queryPageBean.getPageSize();
		String queryString = queryPageBean.getQueryString();//查询条件
		//完成分页查询，基于mybatis框架提供的分页助手插件完成
		PageHelper.startPage(currentPage,pageSize);
		//select * from block limit 0,10
		Page<Post> page =  postMapper.findAllPost(queryString);
		long total = page.getTotal();
		List<Post> rows = page.getResult();
		return new PageResult(total,rows);
	}

	@Override
	public PageResult findByBlock(String block) {
		//完成分页查询，基于mybatis框架提供的分页助手插件完成
		Page<Post> page = postMapper.findByBlock(block);
		long total = page.getTotal();
		List<Post> rows = page.getResult();
		return new PageResult(total,rows);
	}

	/**
	 * 根据id删除帖子,发帖人积分-1
	 * @param post		 帖子
	 * @param integral   积分
	 * @return
	 */
	@Override
	@Transactional
	public boolean deletePost(Post post,Integer integral) {

		//还需要判断当前用户删除的是否是自己的帖子，不可以乱删别人的东西

		if (post.getId()==null){
			return false;
		}
		if (post.getUser_id()==null){
			return false;
		}
		postMapper.deletePost(post.getId());
		userMapper.updateIntegral(post.getUser_id(),integral);
		return true;
	}

	/**
	 * 新建帖子，同时增加10积分
	 * @param post
	 */
	@Override
	@Transactional
	public void createPost(Post post) {
		postMapper.createPost(post);
		userMapper.updateIntegral(post.getUser_id(),10);
	}

}
